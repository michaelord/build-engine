# Prettylittle Client Side Build Engine

A custom build engine written for the Development team. To test run `npm install` and then `node example`.

To integrate with a project *npm install* this package and simply add:

```
// include the plugin
var engine						= require('build-engine');

// start the build tool
engine([settings]);
```

## Config

> TODO
