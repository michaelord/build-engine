'use strict';

// chokidar allows us to watch more relaibily than with node
var chokidar					= require('chokidar'),
	fs							= require('fs'),
	path						= require('path'),
	engine						= require('express-handlebars'),
	express						= require('express'),
	handlebars					= require('handlebars'),
	utils						= require('./utils'),
	bodyParser					= require('body-parser');

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ...
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/**
 * [TemplateEngine description]
 * @param {[type]} options [description]
 */
function TemplateEngine(options)
{
	this.data					= {};

	var _self					= this;

	var DEFAULTS = {
		partials 				: ['src/packages/**/*.html', 'templates/shared/**/*.html'],
		pages					: '/src/pages/',
		masters					: 'src/masters/',
		partials				: [
			'src/packages/libs/',
			'src/partials/',
			'src/shared/'
		],
		//
		modules					: true,
		reports					: true,
		styleguide				: true,
		brand					: true,
		templates				: true,
		cssdocs					: true,
		jsdocs					: true,
		//
		persistent				: true,
		production				: false,
		port					: 4000,

		application				: 'application.json',
		settings				: false,

		params					: {}
	};

	var SETTINGS				= utils.merge({}, DEFAULTS, options);

	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Pages
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	function __getTemplateData(path, stats)
	{
		// normalize the file path
		path					= utils.normalize(path);

		// get the contents of the file
		var contents			= fs.readFileSync(path, 'utf8'),
		// page data object
			data				= {},
		// generate a nice page name
			name				= path.replace(utils.normalize(process.cwd() + SETTINGS.pages), '').replace('.html', ''),
		//
			defaults = {
				"title"			: "untitled",
				"id"			: "",
				"group"			: "default",
				"desc"			: "[desc]",
				"preview"		: [],
				"weight"		: 1,
				"public"		: true
			};

		// if there are contents (there should be)
		if(contents)
		{
			try
			{
				// try to get the page data
				var _data		= contents.match(/\{\{!--([^>])*--\}\}/);

				// create a json object out of the page data
				if(data)
				{
					data		= _data[0].replace('{{!--', '').replace('--}}', '').trim();
					data		= JSON.parse("{" + data + "}");
				}
			}
			catch(e)
			{
				console.log(e);
			}
		}

		data.url				= '/templates/' + name + '.html';
		__page_meta[name]		= utils.merge({}, defaults, data);

		function sortObject(o) {
		    return Object.keys(o).sort().reduce((r, k) => (r[k] = o[k], r), {});
		}

		__page_meta				= sortObject(__page_meta);

	}

	var __page_meta				= {};
	var __pages					= chokidar.watch
	(
		[
			utils.normalize(process.cwd() + SETTINGS.pages) + '*.html',
			utils.normalize(process.cwd() + SETTINGS.pages) + '**/*.html'
		],
	{
		ignored					: /[\/\\]\./,
		persistent				: SETTINGS.persistent
	});

	__pages.on('add', __getTemplateData);
	__pages.on('change', __getTemplateData);

	/**
	 * [getPageData description]
	 * @param  {String} name [description]
	 * @param  {Object} req  [description]
	 * @return {Object}      [description]
	 */
	function getPageData(name, req, params)
	{
		var data =
		{
			request :
			{
				url				: ""
			}
		};

		var settings			= {};

		if(!params)
		{
			params				= {};
		}

		try
		{
			if(__page_meta[name]['master'] && !__page_meta[name]['layout'])
			{
				data['layout']		= __page_meta[name]['master'];
			}

			// create a guid for the page
			data['guid']			= utils.hash(name);

			if(SETTINGS.styleguide || SETTINGS.brand || SETTINGS.docs)
			{
				// TODO: CHANGE THE PATH TO BE DYNAMIC
				data['styles']		= utils.require_uncached(utils.normalize(process.cwd() + "/" + SETTINGS.application));
			}

			if(SETTINGS.settings)
			{
				settings			= utils.require_uncached(utils.normalize(process.cwd() + "/" + SETTINGS.settings));
			}

			if(req)
			{
				data.request.url	= req.path;
				data['path']		= req.originalUrl;
			}
		}
		catch(e)
		{

		}

		return utils.merge({handlebars:handlebars}, settings, _self.data, params, getFrameworkData(), SETTINGS.params, data, __page_meta[name]);
	}

	function getFrameworkData()
	{
		return {
			_usemodules			: SETTINGS.modules,
			_usereports			: SETTINGS.reports,
			_usestyleguide		: SETTINGS.styleguide,
			_usebrand			: SETTINGS.brand,
			_usetemplates		: SETTINGS.templates,
			_usecssdocs			: SETTINGS.cssdocs,
			_usejsdocs			: SETTINGS.jsdocs
		};
	}

	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Helpers
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	var HandlebarsHelpers		= {};

	function __loadHelpers(path, stats)
	{
		// get the data from the file
		var data				= utils.require_uncached(utils.normalize(path));

		// Merge it into the Helpers object
		HandlebarsHelpers		= utils.merge({}, HandlebarsHelpers, data);

		// update the actual handlebars helpers with the functions
		hbs.helpers				= HandlebarsHelpers;
	}

	var __helpers				= chokidar.watch
	(
		[
			utils.normalize(__dirname) + '/helpers/*.js'
		],
	{
		ignored					: /[\/\\]\./,
		persistent				: SETTINGS.persistent
	});

	__helpers.on('add', __loadHelpers);
	__helpers.on('change', __loadHelpers);

	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Server
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	var app						= express();

	var hbs						= engine.create(
		{
			// pass in a new instance of handlebars, this will allow us to use
			// SafeString properly
			handlebars			: handlebars,
			// set the extension name to be html
			extname				: '.html',

			layoutsDir			: SETTINGS.masters,

			// Uses multiple partials dirs, templates in "shared/templates/" are shared with the client-side of the app (see below).
			partialsDir			: SETTINGS.partials,

			// helpers
			helpers				: HandlebarsHelpers,

			// default template
			defaultLayout		: 'master',
		}
	);

	// register file extension mustache
	app.engine('html', hbs.engine);

	// register file extension for partials
	app.set('view engine', 'html');

	//
	app.set('views', process.cwd() + SETTINGS.pages);
	//app.set('views', [__dirname + '/pages/', process.cwd() + SETTINGS.pages]);
	//console.log([__dirname + '/pages/', process.cwd() + SETTINGS.pages]);

	// add assets
	app.use('/assets', express.static(process.cwd() + '/assets'));

	// src
	app.use('/src', express.static(process.cwd() + '/src'));

	//
	app.use('/assets-framework', express.static(__dirname + '/assets-framework'));

	//
	app.use('/placeholder', express.static(__dirname + '/placeholder'));

	// for parsing application/json
	app.use(bodyParser.json());

	// for parsing application/x-www-form-urlencoded
	app.use(bodyParser.urlencoded({ extended: true }));

	// enable caching on production
	if(SETTINGS.production)
	{
		app.enable('view cache');
	}

	function exposePartials(req, res, next)
	{
		hbs.getPartials().then(function(partials)
		{
			// create a new partials object from the partial keys
			var modules = Object.keys(partials).map(function (name)
			{
				return name;
			});

			// sort the partials by length, so we can make sure they should be in hierachy structure
			modules.sort(function(a, b)
			{
				return a.length - b.length;
			});

			// remove diplicates, as we can have shared partials that will take president
			modules = modules.filter(function(item, pos)
			{
				return modules.indexOf(item) == pos;
			});

			var _navigation		= {},
				navigation		= [],
				_sections		= {};


			for(var i = 0; i < modules.length; i++)
			{
				var group		= modules[i];
				var url			= group.toLowerCase();

				if(!_navigation[group])
				{
					_navigation[group]	= true;

					// split the group up by the name (as it may contain a '/')
					var group_parts		= group.split('/');

					if(group_parts.length > 1)
					{
						var group_part	= group_parts[0];

						if(!_sections[group_part])
						{
							_sections[group_part]	= [];
						}

						_sections[group_part].push
						(
							{
								"name"		: group,
								"url"		: '/module/?id=' + url
							}
						)

						if(!_navigation[group_part])
						{
							_navigation[group_part]	= true;

							navigation.push
							(
								{
									"name"		: group_part,
									"url"		: '/module/?id=' + group_part.toLowerCase(),
									"children"	: false
								}
							)
						}
					}
					else
					{
						navigation.push
						(
							{
								"name"		: group,
								"url"		: '/module/?id=' + url,
								"children"	: false
							}
						)
					}
				}
			}

			var compare					= function(a, b)
			{
				if (a.name < b.name)
				{
					return -1;
				}

				if (a.name > b.name)
				{
					return 1;
				}

				return 0;
			};

			navigation.sort(compare);

			for(var i = 0; i < navigation.length; i++)
			{
				if(_sections[navigation[i].name])
				{
					navigation[i].children	= _sections[navigation[i].name].sort(compare);

					for(var j = 0; j < navigation[i].children.length; j++)
					{
						var n			= navigation[i].children[j].name;
						var p			= n.split('/');
							p.shift();

						navigation[i].children[j].display		= p.join(': ');
					}
				}
			}

			// TODO - sort
			//

			res.locals._partials	= navigation;

			setImmediate(next);
		})
		.catch(next);
	}

	app.all('/', function(req, res)
	{
		res.render('_engine/index', getPageData('_engine/index', req));
	});

	if(SETTINGS.modules)
	{
		app.all('/modules', exposePartials, function(req, res)
		{
			res.render('_engine/modules', getPageData('_engine/modules', req, { _is_modules: true }));
		});

		app.all('/module', function(req, res)
		{
			var data			= getPageData('_engine/module', req, { });

			if(req.query['id'])
			{
				data._module_path	= req.query['id'];

				// TODO - get this from the actual partial
				data.module			= {
					title			: "Example Module Title"
				};
			}

			res.render('_engine/module', data);
		});
	}

	if(SETTINGS.reports)
	{
		app.all('/reports', exposePartials, function(req, res)
		{
			res.render('_engine/reports', getPageData('_engine/reports', req, { _is_reports: true }));
		});
	}

	if(SETTINGS.styleguide)
	{
		app.all(/\/styleguide(\/)?(.*)?/, function(req, res)
		{
			var data			= getPageData('_engine/styleguide', req, { _is_styleguide: true });

			if(req.query['tag'])
			{
				data._taglist	= [];
				data._tag		= req.query['tag'];

				if(data.styles.tags[req.query['tag']])
				{
					data._taglist	= data.styles.tags[req.query['tag']];
				}
			}

			res.render('_engine/styleguide', data);
		});
	}

	if(SETTINGS.cssdocs)
	{
		app.all('/docs/css', function(req, res)
		{
			res.render('_engine/docs-css', getPageData('_engine/docs-css', req, { _is_cssdocs: true }));
		});
	}

	if(SETTINGS.jsdocs)
	{
		app.all('/docs/js', function(req, res)
		{
			res.render('_engine/docs-js', getPageData('_engine/docs-js', req, { _is_jsdocs: true }));
		});
	}

	if(SETTINGS.brand)
	{
		app.all('/brand', function(req, res)
		{
			res.render('_engine/brand', getPageData('_engine/brand', req, { _is_brand: true }));
		});
	}

	if(SETTINGS.templates)
	{
		app.all(/\/templates(\/)?(.*)?/, function(req, res)
		{
			var view			= '_engine/templates',
				path			= req.route.path,
				url				= req.originalUrl,
				page_data		= {},
				req_data		= {};

			if(Object.keys(req.body).length !== 0)
			{
				console.log("Request", req.body);

				req_data.formdata	= req.body;
			}

			// if the irl does not have a file extension
			if(url.indexOf('.html') == -1)
			{
				// we're probably requestion in index page
				url				= url + '/index';
			}

			// normalize the url
			url					= utils.normalize(url);

			// remove the templates path, as we're not storing that in the page_meta
			url					= url.replace('/templates/', '').replace('.html', '');

			if(url !== 'index')
			{
				// if the page meta has been loaded...
				if(__page_meta[url])
				{
					view		= url;
				}
				// page does not exist
				else
				{
					req.next();
					return;
				}
			}
			else
			{
				page_data._templates	= __page_meta;

				//
			}

			res.render(view, utils.merge({}, page_data, req_data, getPageData(view, req, { _is_templates: true })));
		});
	}

	/*
	setTimeout(function()
	{
		var d					= getPageData('_engine/index');
			d['layout']			= 'master';
		var p					= hbs.render('src/pages/_engine/index.html', d);

		console.log('##');
		console.log(p);
		console.log('##');

		p.then(function(o)
		{
			console.log(o)
		});
	}, 1000);
	*/

	// any pages that cannot be found, fallback to an error
	app.use(function(req, res, next)
	{
		res.render('_engine/error', utils.merge(
			{
				message			: "We could not find what you where looking for",
				status			: 404
			},
			getPageData('_engine/error', req)
		));
	});

	if(SETTINGS.persistent)
	{
		app.listen(SETTINGS.port, function()
		{
			console.log('Example app listening on port ' + SETTINGS.port);
		});
	}
}

TemplateEngine.example = function()
{
	console.log("TemplateEngine.example()");
}

module.exports					= TemplateEngine;
