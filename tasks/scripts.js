

var config_file					= process.cwd() + "/src/config.json";

if(process.env.APP_CONFIG)
{
	config_file					= process.env.APP_CONFIG;
}

var config						= require(config_file)['scripts'];

// *****************************************************************************
// Libraries
// *****************************************************************************

var gulp						= require('gulp'),
	sourcemaps					= require('gulp-sourcemaps'),
	source						= require('vinyl-source-stream'),
	buffer						= require('vinyl-buffer'),
	browserify					= require('browserify'),
	watchify					= require('watchify'),
	babel						= require('babelify'),
	esdoc						= require("gulp-esdoc"),
	watch						= require('gulp-watch'),
	notify						= require('gulp-notify'),
	plumber						= require('gulp-plumber'),
	gulpif						= require('gulp-if'),
	rename						= require('gulp-rename'),
	del							= require('del'),
	//
	uglify						= require('gulp-uglify');

// *****************************************************************************
// Tasks
// *****************************************************************************

gulp.task('scripts', function()
	{
		var bundler	= browserify('./src/packages/application.js',
			{
				debug			: false
			}
		).transform
		(
			babel.configure
			(
				{
					// Use all of the ES2015 spec
					presets		: ["es2015"]
				}
			)
		);

		var plumberErrorHandler	= {
			errorHandler		:notify.onError(
				{
					title		:"Gulp",
					message		:"Error: <%= error.message %>"
				}
			)
		};

		return bundler.bundle()
			.pipe(plumber(plumberErrorHandler))
			//
			//.pipe(sourcemaps.init())
			//
			.pipe(source('application.js'))
			// Save off the uncompressed JS
			.pipe(gulpif(config.production, gulp.dest(config.dest)))
			// Rename the output file to a minified extension
			.pipe(rename({extname:'.min.js'}))
			// Compress the JS
			.pipe(gulpif(config.production, uglify(
				{
					// http://lisperator.net/uglifyjs/compress
					compress :
					{
						global_defs :
						{
							DEBUG		: false
						}
					}
				}
			)))
			//
			//.pipe(sourcemaps.write('.'))
			// Save off the minified file
			.pipe(gulp.dest(config.dest));
/*
		var extensions 			= ['.js', '.jsx', '.ts', '.tsx'];

		var bundler				= browserify("./src/packages/application.ts",
			{
				debug			: true
			}
		)
		.plugin(tsify, { target: 'es6' })
		.transform(babelify.configure(
			{
				extensions		: extensions
			}
		));

		var plumberErrorHandler	= {
			errorHandler	:notify.onError(
				{
					title	:"Gulp",
					message	:"Error: <%= error.message %>"
				}
			)
		};

		return bundler.bundle()
			// Plumber to stop errors killing the process
				.pipe(plumber(plumberErrorHandler))
				.pipe(source('application.js'))
				.pipe(buffer())
				// Save off the uncompressed JS
				.pipe(gulpif(config.production, gulp.dest(config.dest)))
				// Rename the output file to a minified extension
				.pipe(rename({extname:'.min.js'}))
				// Compress the JS
				.pipe(gulpif(config.production, uglify()))
				// Save off the minified file
				.pipe(gulp.dest(config.dest));
				*/
	}
);

// watch all the files, using gulp-watch rather than gulp.watch, as it's much more reliable
gulp.task('scripts:watch', function()
{
	watch(config.paths, function(vinyl)
	{
		gulp.start.apply(gulp, ['scripts']);
	});
});

// ...
gulp.task('scripts:doc', function()
{
	gulp.src("./src/packages")
		.pipe(esdoc(
			{
				destination		: "./documentation",
				excludes		: ["lib/*.js"],
				lint			: false,
				coverage		: false
			}
		));
});
