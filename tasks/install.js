// *****************************************************************************
// Libraries
// *****************************************************************************

var gulp						= require('gulp'),
	install						= require('gulp-install');

// *****************************************************************************
// Tasks
// *****************************************************************************

gulp.task('install',
	/**
	 * [description]
	 * @return {[type]} [description]
	 */
	function()
	{
		// install bower and npm packages, bower package installation path is defined by the config in .bowerrc
		return gulp
			.src(['./bower.json', './package.json'])
			.pipe(install());
	}
);
