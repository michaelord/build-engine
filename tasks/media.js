

var config_file					= process.cwd() + "/src/config.json";

if(process.env.APP_CONFIG)
{
	config_file					= process.env.APP_CONFIG;
}

var config						= require(config_file)['media'];

// *****************************************************************************
// Libraries
// *****************************************************************************

var gulp						= require('gulp')
	svgmin						= require('gulp-svgmin');
	/*
	newer						= require('gulp-newer'),
	png							= require('imagemin-optipng'),
	jpg							= require('imagemin-jpegtran'),
	gif							= require('imagemin-gifsicle');
*/
// *****************************************************************************
// Tasks
// *****************************************************************************

gulp.task('media:icons', function()
	{
		var svg_config		= require(process.env.INIT_CWD + '/gulp/config/svgmin.json');

		return gulp
			.src(config.build_paths)
			.pipe(svgmin(svg_config))
			// Save off the minified file
			.pipe(gulp.dest(config.dest));
	}
);

gulp.task('media', [/*"media:webp", *//*"media:jpg"*//*, "media:gif", "media:png", "media:svg"*/"media:icons"]);

gulp.task('media:jpg',
	/**
	 * [description]
	 * @return {[type]} [description]
	 */
	function()
	{
		//
		return gulp.src
			(
				[
					config.path + "*.jpg",
					config.path + "**/*.jpg"
				]
			)
			.pipe
			(
				jpg
				(
					{
						progressive		: true,
						arithmetic		: false
					}
				)()
			)
			.pipe(gulp.dest(config.dest));
	}
);

// watch all the files, using gulp-watch rather than gulp.watch, as it's much more reliable
gulp.task('media:watch', function()
	{
		/*
		watch(config.paths, function(vinyl)
		{
			gulp.start.apply(gulp, ['media']);
		});
		*/
	}
);
