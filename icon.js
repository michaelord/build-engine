'use strict';

// chokidar allows us to watch more relaibily than with node
var chokidar					= require('chokidar'),
	fs							= require('fs'),
	utils						= require('./utils'),
	path						= require('path');

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ...
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

String.prototype.toProperCase = function () {
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

/**
 * [TemplateEngine description]
 * @param {[type]} options [description]
 */
function Icons(options)
{
	var DEFAULTS = {
		dir						: '/src/icons/',
		ext						: '.svg',
		persistent				: true,
		output					: 'icon.json',
		onupdate				: null
	};

	var SETTINGS				= utils.merge({}, DEFAULTS, options);

	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Icons
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	var __icon_meta				= {};
	var __icons					= chokidar.watch
	(
		[
			utils.normalize(process.cwd() + SETTINGS.dir) + '*' + SETTINGS.ext,
			utils.normalize(process.cwd() + SETTINGS.dir) + '**/*' +  + SETTINGS.ext
		],
	{
		ignored					: /[\/\\]\./,
		persistent				: SETTINGS.persistent
	});

	var __save_interval			= null;

	var __save = function()
	{
		var output				= JSON.stringify(__icon_meta, null, ' ');

		fs.writeFile(utils.normalize(process.cwd()) + '/' + SETTINGS['output'], output);

		if(SETTINGS.onupdate)
		{
			setTimeout(function()
			{
				SETTINGS.onupdate();
			}, 600);
		}
	}

	__icons.on('add', function(p, stats)
	{
		// normalize the file path
		p						= utils.normalize(p);

		var name				= path.basename(p).replace(/\.[^.]*$/, '');
		var filepath			= SETTINGS.dir + (p.split(SETTINGS.dir)[1]);

		__icon_meta[name]		= {
			path				: filepath,
			name				: name,
			display				: name.replaceAll('-', ' ').toProperCase()
		};

		clearTimeout(__save_interval);

		__save_interval			= setTimeout(__save, 100);
	});

	__icons.on('unlink', function(p, stats)
	{
		// normalize the file path
		p						= utils.normalize(p);

		var name				= path.basename(p).replace(/\.[^.]*$/, '');

		delete __icon_meta[name];

		clearTimeout(__save_interval);

		setTimeout(__save, 100);
	});
}

module.exports					= Icons;
