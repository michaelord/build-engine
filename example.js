'use strict';



var engine						= require('./index');

engine
(
	{
		persistent				: true,

		modules					: true,
		templates				: true,
		// config needed
		reports					: true,
		styleguide				: true,
		brand					: true,
		docs					: true,
		//
		port					: 3020,

		application				: "application.json",

		params				: {
			"site-name"			: "TH_NK",
			"username"			: "[username]"
		}
	}
);

var styleguide					= require('./styleguide');

styleguide
(
	{
		styles					: '/src/packages/',
		output					: 'application.json',
		persistent				: true,
		main					: "application.scss"
	}
);
