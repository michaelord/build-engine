
var getModifiers		= require('./modifiers').modifiers;

exports.styleguide = function(options)
{
	var defaults	= {
		title		: "No title",
		modifiers	: ''
	};

	var params					= Object.assign({}, defaults, options.hash);
	var modifiers				= getModifiers('fw-card', 'example,' + params.modifiers, options);
	var out						= '<div class="'+modifiers+'"><dl class="fw-details"><dt>'+params.title+'</dt></dl>'+options.fn()+'</div>';

	return out;
}

String.prototype.toProperCase = function ()
{
	return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

exports.tidyname = function(a, b, options)
{
	var name					= b;

	if(a)
	{
		name					= a;
	}

	name						= name.replace(/-/g, " ");

	return name.toProperCase();
}
