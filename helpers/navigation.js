
var getModifiers		= require('./modifiers').modifiers;
var getIcon				= require('./icon').icon;

var getAttributes = function(atts)
{
	var _return			= '';

	for(var i in atts)
	{
		if(atts[i])
		{
			_return   += ' ' + i + '="' + atts[i] + '" ';
		}
	}

	return _return;
}

exports.nav = function()
{
	// get the arguments as an array so we can manipulate it
	var args		= Array.prototype.slice.call(arguments);

	// options is always the last element in the array
	var options		= args.pop();

	var nav			= args[0];
	var modifiers	= args[1] || '';

	// if there is no navigation...
	if(!nav)
	{
		return "";
	}

	// prefix for the navigation list
	var prefix		= 'nav';
	var icon_more	= getIcon('more', options);

	var generate	= function(items, level)
	{
		var _return		= '';

		for(var i = 0; i < items.length; i++)
		{
			var item		= items[i];
			var children	= item.children,
				child_html	= '';

			if(children)
			{
				child_html	= generate(children, level + 1);
			}

			var anchor_atts	= {
				"href"		: item["url"],
				"target"	: item["target"],
			};

			var li_classes	= [prefix + '__item'];

			if(child_html)
			{
				li_classes.push(prefix + '__children');
			}

			if(options.data.root.request.url && item["url"])
			{
				if(options.data.root.request.url.replace('/templates/', '') === item["url"].replace('/templates/', ''))
				{
					li_classes.push(prefix + '__active');
				}
			}

			var inner		= '';

			if(item.icon)
			{
				li_classes.push(prefix + '__item-icon');

				inner	   += getIcon(item.icon, 'sm', options);
			}

			inner		   += '<span class="'+prefix+'__text">' + item.name + '</span>';

			if(child_html)
			{
				inner	   += icon_more;
			}

			// start the opening list item
			_return		   += '<li class="' + li_classes.join(' ') + '">';

			//
			if(anchor_atts["href"])
			{
				_return	   += '<a' + getAttributes(anchor_atts) + '>' + inner + '</a>';
			}
			else
			{
				_return	   += '<span class="a">' + inner + '</span>';
			}

			// if there are children, add them to the end of the list
			if(child_html)
			{
				_return   += '<ul class="'+prefix+'__level '+prefix+'__level-'+(level + 1)+'">' + child_html + '</ul>';
			}

			// close the list itsm
			_return		   += '</li>';
		}

		return _return;
	};

	var str			= '<nav class="'+getModifiers(prefix, modifiers, options)+'"><ul class="'+prefix+'__level '+prefix+'__level-1">' + generate(nav, 1) + '</ul></nav>';

	return new options.data.root.handlebars.SafeString(String(str));
}
