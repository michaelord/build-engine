
exports.times = function(n, options)
{
	var out		= '';

	for(var i = 0; i < n; ++i)
	{
		out	   += options.fn(i);
	}

	return out;
};
