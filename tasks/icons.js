

var config_file					= process.cwd() + "/src/config.json";

if(process.env.APP_CONFIG)
{
	config_file					= process.env.APP_CONFIG;
}

var config						= require(config_file)['icons'];

// *****************************************************************************
// Libraries
// *****************************************************************************

var gulp						= require('gulp'),
	watch						= require('gulp-watch'),
	cheerio						= require('gulp-cheerio'),
	svgstore					= require('gulp-svgstore'),
	svgmin						= require('gulp-svgmin'),
	path						= require('path');

// *****************************************************************************
// Tasks
// *****************************************************************************

gulp.task('icons', function()
	{
		return gulp
			.src(config.build_paths)
			.pipe(svgmin(function (file)
			{
	            var prefix		= path.basename(file.relative, path.extname(file.relative));

	            return {
	                plugins		:
	                [
		                {
		                    cleanupIDs	:
		                    {
		                        prefix	: prefix + '-',
		                        minify	: true
		                    }
		                }
					]
	            }
	        }))
			.pipe(cheerio(
			{
				run: function ($)
				{
					// Remove fill attribute to allow total CSS control
					$('[fill]').removeAttr('fill');
					$('[id]').removeAttr('id');
				},
				parserOptions	:
				{
					xmlMode		: true
				}
			}))
			.pipe(svgstore(

			))
			.pipe(gulp.dest(config.dest));
	}
);

// watch all the files, using gulp-watch rather than gulp.watch, as it's much more reliable
gulp.task('icons:watch', function()
	{
		watch(config.paths, function(vinyl)
		{
			gulp.start.apply(gulp, ['icons']);
		}
	);
});

