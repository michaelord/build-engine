
var l							= require('./lib/lorem');

exports.lorem = function(items, options)
{
	try
	{
		if(arguments.length === 1)
		{
			options				= items;
			items				= 'p';
		}

		var html				= l.ipsum(items);

		if(html)
		{
			return new options.data.root.handlebars.SafeString(String(html));
		}
	}
	catch(e)
	{

	}

	return '';
};
