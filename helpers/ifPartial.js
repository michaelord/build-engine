
exports.ifPartial = function(name, options)
{
	return options.data.exphbs.partials[name] ? options.fn(this) : options.inverse(this);
};
