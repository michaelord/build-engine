
var config_file					= process.cwd() + "/src/config.json";

if(process.env.APP_CONFIG)
{
	config_file					= process.env.APP_CONFIG;
}

var config						= require(config_file)['styles'];

// *****************************************************************************
// Libraries
// *****************************************************************************

var gulp						= require('gulp'),
	watch						= require('gulp-watch'),
	sass						= require('gulp-sass'),
	postcss						= require('gulp-postcss'),
	autoprefixer				= require('autoprefixer'),
	rename						= require('gulp-rename'),
	mqpacker					= require("css-mqpacker"),
	csscomb						= require('gulp-csscomb'),
	sourcemaps					= require('gulp-sourcemaps'),
	cssnano						= require('cssnano'),
	notify						= require('gulp-notify'),
	gulpif						= require('gulp-if'),
	plumber						= require('gulp-plumber');

// *****************************************************************************
// Tasks
// *****************************************************************************

gulp.task('styles', function()
	{
		// plumber error handler to stop things from breaking on errors
		var plumberErrorHandler	= {
			errorHandler : notify.onError
			(
				{
					title		: "Gulp",
					message		: "Error: <%= error.message %>"
				}
			)
		};

		var process_autoprefix	= autoprefixer
		(
			{
				browsers 		: config.autoprefix_browsers,
				cascade 		: false
			}
		);

		var process_nano		= cssnano
		(
			{
				autoprefixer	: false
			}
		);

		var processors			= [];
			processors.push(mqpacker);

			// Add autoprefix post CSS
			processors.push(process_autoprefix);

		return gulp.src(config.build_paths)
			// Plumber to stop errors killing the process
			.pipe(plumber(plumberErrorHandler))
			//
			.pipe(sourcemaps.init())
			// SASS this bad boy
			.pipe(sass
				(
					{
						outputStyle		: "expanded",
						errLogToConsole	: true,
						indentType 		: 'tab',
						indentWidth		: 1,
					}
				)
			)
			// Format
			.pipe(csscomb())
			// run the core CSS PostProcesses
			.pipe(postcss(processors))
			// Save off the uncompressed CSS
			//.pipe(gulpif(config.production, gulp.dest(config.dest)))
			// Minify the CSS
			.pipe(gulpif(config.production, postcss([process_nano])))
			// Rename the output file to a minified extension
			.pipe(rename({extname:'.min.css'}))
			//
			//.pipe(sourcemaps.write('.'))
			// Save off the minified file
			.pipe(gulp.dest(config.dest));
	}
);

// watch all the files, using gulp-watch rather than gulp.watch, as it's much more reliable
gulp.task('styles:watch', function()
	{
		watch(config.paths, function(vinyl)
		{
			gulp.start.apply(gulp, ['styles']);
		}
	);
});
