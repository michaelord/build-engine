
var getModifiers	= require('./modifiers').modifiers;

exports.icon = function()
{
	try
	{
		var args		= Array.prototype.slice.call(arguments);
		var options		= args.pop();

		var icon		= args[0];
		var modifiers	= args[1] || '';
		var classes		= args[2] || '';

			classes		= getModifiers('icon', icon + (modifiers ? ',' : '') + modifiers, options) + (classes ? (' ' + classes) : '');

		var str			= '<i aria-hidden="true" class="'+classes+'"><svg><use xlink:href="/assets/media/icons.svg#'+icon+'"></use></svg></i>';

		return new options.data.root.handlebars.SafeString(String(str));
	}
	catch(e)
	{
		console.warn("Error: helpers/icon.js");
	}

	return '';
}
