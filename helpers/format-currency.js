
exports['format-currency'] = function()
{
	var args					= Array.prototype.slice.call(arguments);
	var options					= args.pop();

	var value					= args[0];
	var pounds					= '0';
	var pence					= '00';
	var currency				= "£";

	if(value)
	{
		// TODO: make this into an arg
		value					= value.toLocaleString();

		value					= String(value);

		if(value.indexOf('.') > -1)
		{
			pounds				= value.split('.')[0];
			pence				= Number(value.split('.')[1]);

			if(pence < 10)
			{
				pence			= pence + '0';
			}
		}
		else
		{
			pounds				= value;
		}
	}

	return new options.data.root.handlebars.SafeString('<span class="currency currency--lg">'+currency+pounds+'<span class="currency__digits">.'+pence+'</span></span>');
};
