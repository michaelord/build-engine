
exports.random = function(min, max, options)
{
	if(arguments.length !== 3)
	{
		min						= 1;
		max						= 1000;
	}

	return Math.floor(Math.random() * max) + min;
}
